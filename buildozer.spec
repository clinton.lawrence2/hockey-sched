[app]
title = Sports Schedule Updater
package.name = sports
package.domain = org.test
source.dir = .
source.include_exts = py,png,jpg,kv,atlas,json,txt,yaml,pickle
version = 0.1
requirements = cachetools==5.3.3,certifi==2024.2.2,charset-normalizer==3.3.2,Cython==0.29.33,docutils==0.21.2,google-api-core==2.19.0,google-api-python-client==2.131.0,google-auth==2.29.0,google-auth-httplib2==0.2.0,google-auth-oauthlib==1.2.0,googleapis-common-protos==1.63.0,httplib2==0.22.0,idna==3.7,Kivy==2.3.0,Kivy-Garden==0.1.5,oauthlib==3.2.2,proto-plus==1.23.0,protobuf==4.25.3,pyaml==24.4.0,pyasn1==0.6.0,pyasn1_modules==0.4.0,Pygments==2.18.0,pyparsing==3.1.2,python-dateutil==2.9.0.post0,pytz==2024.1,PyYAML==6.0.1,requests==2.32.3,requests-oauthlib==2.0.0,rsa==4.9,six==1.16.0,uritemplate==4.1.1,urllib3==2.2.1,
orientation = portrait
android.permissions = INTERNET, WRITE_EXTERNAL_STORAGE, WAKE_LOCK
android.api = 31
android.minapi = 21
android.ndk = 25b
android.sdk = 20
android.wakelock = True
android.archs = arm64-v8a, armeabi-v7a
android.allow_backup = True
android.enable_androidx = True

[buildozer]
log_level = 2
warn_on_root = 1
