from datetime import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import logging
from pytz import timezone

# logging config to stdout
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())

SCOPES = ['https://www.googleapis.com/auth/calendar']
CREDENTIALS_FILE = "googleCreds.json"

def get_calendar_service():
    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CREDENTIALS_FILE, SCOPES)
            creds = flow.run_local_server(port=0)
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    service = build('calendar', 'v3', credentials=creds)
    return service

def addEvent(calService, calId, gameData):
    event = {
        'summary': gameData.get('summary', 'Game'),  # Add fallback value
        'start': {
            'dateTime': gameData.get('start'),
            'timeZone': 'America/Phoenix'  # Adjust timezone as needed
        },
        'end': {
            'dateTime': gameData.get('end'),
            'timeZone': 'America/Phoenix'
        },
        'description': gameData.get('description', '')
    }
    
    # Return the request object, not the event dictionary
    return calService.events().insert(calendarId=calId, body=event)


def createCal(service, name):
    calendars_result = service.calendarList().list().execute()
    calendar_id = None
    calendars = calendars_result.get('items', [])
    calExists = False

    for calendar in calendars:
        summary = calendar['summary']
        if summary == name:
            calExists = True
            calendar_id = calendar['id']
            logger.info(f"Calendar {name} already exists. Clearing its events.")
            break

    if not calendar_id:
        calendar = {
            'summary': name,
            'timeZone': 'Etc/UTC'
        }
        logger.info(f"Calendar: {name} Doesnt Exist. Creating")
        created_calendar = service.calendars().insert(body=calendar).execute()
        calendar_id = created_calendar['id']

    return calendar_id,calExists

def getEvents(service, calId):
    page_token = None
    eventLabel = []

    while True:
        events = service.events().list(calendarId=calId, pageToken=page_token).execute()
        for event in events['items']:
            gameIdTitle, sep, _ = event["summary"].partition('(')
            gameIdTitle = gameIdTitle.strip()
            gameStartTime = datetime.strptime(event["start"]["dateTime"], '%Y-%m-%dT%H:%M:%SZ').replace(tzinfo=timezone('UTC'))
            gameIdDate  = gameStartTime.strftime("%m-%d-%Y")
            eventLabel.append((gameIdDate + "_" + gameIdTitle).replace(" ","_"))
        page_token = events.get('nextPageToken')
        if not page_token:
            return eventLabel
